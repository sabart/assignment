# Assignment

## Running locally

A MySQL Docker container is provided to run the application locally.

The script `start-local.sh` will start the Docker container and the Spring Boot
application. The Docker container is taken down on process exiting (`Ctrl-C`).

## Tests

Tests with the persistence layer mocked (Mockito) are implemented in `OrderTests` and
`ProductTests`.

Integration tests with a dockerized MySQL and 
[Testcontainers](https://www.testcontainers.org/) framework would 
be more useful as it's the only way to test Database logic.

## API documentation

API documentation is implemented in OpenAPI (it may be incomplete).

To access the Swagger UI, start the application locally and point your browser
to [http://localhost:8080/swagger-ui.html]().

## Security

Nowadays, APIs are typically secured with OAuth which also allows reusing 
social apps as authentication providers.

An older simple approach would be HTTP auth over SSL.

## Design decisions

Validation is done manually at the service layer with the help of a global exception
handler. Java Validation API is an alternative worth exploring.

DTOs are always used to wrap entities even in simple cases as experience
shows that in real scenarios responses require more attributes than the ones
provided by the entities.

Lombok is used where it's helpful.


 
