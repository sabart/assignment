#!/bin/bash

trap "docker-compose down" EXIT

docker-compose up -d
echo "Waiting 8 seconds for mysql to start up ..."
sleep 8
./mvnw spring-boot:run

