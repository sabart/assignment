package test.assignment.service.order;

import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import test.assignment.TestsSupport;
import test.assignment.service.Money;
import test.assignment.service.order.Order;
import test.assignment.service.product.Product;
import test.assignment.service.order.CreateOrderRequest;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Optional;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@EnableAutoConfiguration(exclude = {
        DataSourceAutoConfiguration.class,
        DataSourceTransactionManagerAutoConfiguration.class,
        HibernateJpaAutoConfiguration.class
})
class OrderTests extends TestsSupport {

    @Captor
    ArgumentCaptor<Product> productCaptor;

    @Captor
    ArgumentCaptor<Order> orderCaptor;

    @Test
    void testCreateANewOrder() throws Exception {
        byte[] resource = readResource("order-request.json");
        final CreateOrderRequest givenCreateOrderRequest = mapper.readValue(new String(resource), CreateOrderRequest.class);
        final long orderId = 1L;
        final String expectedLocation = "http://localhost/order/" + orderId;

        Product p1 = new Product(1L, "Some name #1", new Money(new BigDecimal(100), "EUR"));
        Product p2 = new Product(2L, "Some name #2", new Money(new BigDecimal(50), "EUR"));
        Product p3 = new Product(5L, "Some name #5", new Money(new BigDecimal(50), "EUR"));
        when(productRepository.findById(1L)).thenReturn(Optional.of(p1));
        when(productRepository.findById(2L)).thenReturn(Optional.of(p2));
        when(productRepository.findById(5L)).thenReturn(Optional.of(p3));
        Order mockOrder = mock(Order.class);
        when(mockOrder.getId()).thenReturn(orderId);
        when(orderRepository.save(any(Order.class))).thenReturn(mockOrder);

        mockMvc.perform(post("/order").content(resource).contentType("application/json"))
                .andExpect(status().is(201)).andExpect(header().string("Location", expectedLocation));
        verify(orderRepository, times(1)).save(any(Order.class));
        verify(orderRepository).save(orderCaptor.capture());
        final Order savingOrder = orderCaptor.getValue();

        assertNull(savingOrder.getId());
        assertNotNull(savingOrder.getCreatedDate());
        assertEquals(givenCreateOrderRequest.getBuyerEmail(), savingOrder.getBuyerEmail());
        assertEquals(new Money(new BigDecimal(200), "EUR"), savingOrder.getTotalPrice());
        assertThat(savingOrder.getProducts().contains(p1));
        assertThat(savingOrder.getProducts().contains(p2));
        assertThat(savingOrder.getProducts().contains(p3));
    }

    @Test
    void testCreateANewOrder_InvalidBuyerEmail() throws Exception {
        byte[] resource = readResource("order-request-invalid-buyer-email.json");

        mockMvc.perform(post("/order").content(resource).contentType("application/json"))
                .andExpect(status().is(400))
                .andExpect(jsonPath("$.errorCode", is("invalid.attribute")));
        verify(orderRepository, times(0)).save(any(Order.class));
    }

    @Test
    void testFindOrderByDate() throws Exception {

        Product p1 = new Product(1L, "Some name #1", new Money(new BigDecimal(100), "EUR"));
        Product p2 = new Product(2L, "Some name #2", new Money(new BigDecimal(50), "EUR"));
        Product p3 = new Product(5L, "Some name #5", new Money(new BigDecimal(50), "EUR"));
        Order mockOrder = new Order(1L,"someone@somehere.com", ZonedDateTime.now(), asList(p1, p2, p3), new Money(new BigDecimal(200), "EUR"));


        when(orderRepository.findAllByCreatedDateBetween(any(ZonedDateTime.class), any(ZonedDateTime.class))).thenReturn(singletonList(mockOrder));

        mockMvc.perform(get("/order")
                .param("fromDate", "2020-10-26T21:05:13.542+01:00")
                .param("toDate", "2020-10-27T21:05:13.542+01:00"))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$.orders.length()", is(1)))
                .andExpect(jsonPath("$.orders.[0].totalPrice.amount", is(200)));

    }

}
