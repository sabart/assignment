package test.assignment.service.product;

import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import test.assignment.TestsSupport;
import test.assignment.service.Money;
import test.assignment.service.product.Product;
import test.assignment.service.product.CreateModifyProductRequest;

import java.math.BigDecimal;
import java.util.Arrays;

import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@EnableAutoConfiguration(exclude = {
        DataSourceAutoConfiguration.class,
        DataSourceTransactionManagerAutoConfiguration.class,
        HibernateJpaAutoConfiguration.class
})
class ProductTests extends TestsSupport {

    @Captor
    ArgumentCaptor<Product> productCaptor;

    @Test
    void testCreateANewProduct() throws Exception {
        byte[] resource = readResource("product-request.json");
        final CreateModifyProductRequest givenProductRequest = mapper.readValue(new String(resource), CreateModifyProductRequest.class);
        final long productId = 1L;
        final String expectedLocation = "http://localhost/product/" + productId;

        Product mockProduct = mock(Product.class);
        when(mockProduct.getId()).thenReturn(productId);
        when(productRepository.save(any(Product.class))).thenReturn(mockProduct);

        mockMvc.perform(post("/product").content(resource).contentType("application/json"))
                .andExpect(status().is(201))
                .andExpect(header().string("Location", expectedLocation));
        verify(productRepository, times(1)).save(any(Product.class));
        verify(productRepository).save(productCaptor.capture());
        final Product savingProduct = productCaptor.getValue();

        assertNull(savingProduct.getId());
        assertEquals(givenProductRequest.getName(), savingProduct.getName());
        assertEquals(givenProductRequest.getPrice(), savingProduct.getPrice());
    }

    @Test
    void testCreateANewProduct_MissingProperties() throws Exception {

        mockMvc.perform(post("/product").content("{}").contentType("application/json"))
                .andExpect(status().is(400))
                .andExpect(jsonPath("$.errorCode", is("missing.attribute")));
        verify(productRepository, times(0)).save(any(Product.class));
    }

    @Test
    void testModifyExistingProduct() throws Exception {
        long givenProductId = 2L;
        byte[] resource = readResource("product-request.json");
        final CreateModifyProductRequest givenProductRequest = mapper.readValue(new String(resource), CreateModifyProductRequest.class);
        when(productRepository.existsById(givenProductId)).thenReturn(true);

        mockMvc.perform(put("/product/" + givenProductId).content(resource).contentType("application/json"))
                .andExpect(status().is(204));

        verify(productRepository, times(1)).save(any(Product.class));
        verify(productRepository).save(productCaptor.capture());
        final Product updatingProduct = productCaptor.getValue();

        assertEquals(givenProductId, updatingProduct.getId());
        assertEquals(givenProductRequest.getName(), updatingProduct.getName());
        assertEquals(givenProductRequest.getPrice(), updatingProduct.getPrice());
    }

    @Test
    void testModifyMissingProduct() throws Exception {
        long givenProductId = 2L;
        byte[] resource = readResource("product-request.json");
        final CreateModifyProductRequest givenProductRequest = mapper.readValue(new String(resource), CreateModifyProductRequest.class);
        when(productRepository.existsById(givenProductId)).thenReturn(false);

        mockMvc.perform(put("/product/" + givenProductId).content(resource).contentType("application/json"))
                .andExpect(status().is(404))
                .andExpect(jsonPath("$.errorCode", is("resource.not_found")));

        verify(productRepository, times(0)).save(any(Product.class));
    }

    @Test
    void testModifyANewProduct_MissingProperties() throws Exception {

        long givenProductId = 2L;
        when(productRepository.existsById(givenProductId)).thenReturn(true);

        mockMvc.perform(put("/product/" + givenProductId).content("{}").contentType("application/json"))
                .andExpect(status().is(400))
                .andExpect(jsonPath("$.errorCode", is("missing.attribute")));
        verify(productRepository, times(0)).save(any(Product.class));
    }

    @Test
    void testRetrieveAllProducts() throws Exception {
        Product p1 = new Product(1L, "Some name #1", new Money(new BigDecimal(100.5), "EUR"));
        Product p2 = new Product(2L, "Some name #2", new Money(new BigDecimal(110.5), "GBP"));
        Product p3 = new Product(3L, "Some name #3", new Money(new BigDecimal(120.5), "USD"));
        when(productRepository.findByOrderByName()).thenReturn(Arrays.asList(p1, p2, p3));

        mockMvc.perform(get("/product"))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$.products.length()", is(3)))
                // Do more assertions
                .andExpect(jsonPath("$.products.[1].name", is("Some name #2")));

        verify(productRepository, times(1)).findByOrderByName();
    }

}
