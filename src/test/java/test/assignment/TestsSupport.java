package test.assignment;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.io.ResourceLoader;
import org.springframework.test.web.servlet.MockMvc;
import test.assignment.service.order.OrderRepository;
import test.assignment.service.product.ProductRepository;

import java.io.IOException;
import java.io.InputStream;

import static org.mockito.Mockito.reset;

@SpringBootTest
@AutoConfigureMockMvc
public abstract class TestsSupport {

    @Autowired
    protected MockMvc mockMvc;

    @Autowired
    protected ObjectMapper mapper;

    @Autowired
    private ResourceLoader resourceLoader;

    @MockBean
    protected ProductRepository productRepository;

    @MockBean
    protected OrderRepository orderRepository;

    @BeforeEach
    public void beforeEach() {
        reset(productRepository);
        reset(orderRepository);
    }

    protected byte[] readResource(String resourceName) throws IOException {
        InputStream inputStream = resourceLoader.getResource("classpath:" + resourceName).getInputStream();
        return IOUtils.toByteArray(inputStream);
    }


}
