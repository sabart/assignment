#!/bin/bash

curl -w "\n" -v \
            -X POST --header "Content-Type: application/json" \
            http://localhost:8080/order \
            -d '{"buyerEmail": "someone@somehere.com", "productIds": [1,2,3]}'