#!/bin/bash

curl -w "\n" -v \
            -X POST --header "Content-Type: application/json" \
            http://localhost:8080/product \
            -d '{"name": "Some name", "price": {"amount": 26.9, "currency":"EUR"}}'