#!/bin/bash

curl -v -w "\n" \
            -X PUT --header "Content-Type: application/json" \
            http://localhost:8080/product/2 \
            -d '{"name": "Some name #2", "price": {"amount": 30.0, "currency":"EUR"}}'