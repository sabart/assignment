package test.assignment.controller;

public class CheckedErrorResponse extends ErrorResponse {

    public CheckedErrorResponse(String errorCode) {
        super(errorCode, null, null);
    }

    public CheckedErrorResponse(String errorCode,String... params) {
        super(errorCode, null, params);
    }

}
