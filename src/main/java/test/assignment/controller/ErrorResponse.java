package test.assignment.controller;

import lombok.Getter;
import lombok.ToString;

import java.util.List;

@Getter
@ToString
public abstract class ErrorResponse {

    private final String errorCode;

    private final String message;

    private final String[] parameters;

    ErrorResponse(String errorCode, String message, String[] parameters) {
        this.errorCode = errorCode;
        this.message = message;
        this.parameters = parameters;
    }

}
