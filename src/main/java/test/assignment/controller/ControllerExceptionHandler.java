package test.assignment.controller;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import test.assignment.service.InvalidAttributeException;
import test.assignment.service.InvalidCurrency;
import test.assignment.service.MissingAttributeException;
import test.assignment.service.ResourceNotFoundException;

@ControllerAdvice
public class ControllerExceptionHandler
        extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value
            = {ResourceNotFoundException.class})
    protected ResponseEntity<Object> handleException(
            ResourceNotFoundException ex, WebRequest request) {
        return handleExceptionInternal(ex, new CheckedErrorResponse("resource.not_found", "type=" + ex.getType(), "id=" + ex.getId()),
                new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

    @ExceptionHandler(value
            = {MissingAttributeException.class})
    protected ResponseEntity<Object> handleException(
            MissingAttributeException ex, WebRequest request) {
        return handleExceptionInternal(ex, new CheckedErrorResponse("missing.attribute", ex.getAttribute()),
                new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler(value
            = {InvalidAttributeException.class})
    protected ResponseEntity<Object> handleException(
            InvalidAttributeException ex, WebRequest request) {
        return handleExceptionInternal(ex, new CheckedErrorResponse("invalid.attribute", ex.getAttribute()),
                new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler(value
            = {InvalidCurrency.class})
    protected ResponseEntity<Object> handleException(
            InvalidCurrency ex, WebRequest request) {
        return handleExceptionInternal(ex, new CheckedErrorResponse("invalid.currency",
                        "invalid=" + ex.getInvalidCurrency(),
                        "valid=" + ex.getValidCurrency(),
                        "product.id=" + ex.getProductId()),
                new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler(value
            = {Exception.class})
    protected ResponseEntity<Object> handleGeneralException(
            Exception ex, WebRequest request) {
        ex.printStackTrace();
        String message = ExceptionUtils.getRootCauseMessage(ex);
        return handleExceptionInternal(ex, new GenericErrorResponse(message),
                new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }
}