package test.assignment.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.headers.Header;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import test.assignment.service.order.Order;
import test.assignment.service.*;
import test.assignment.service.order.CreateOrderRequest;
import test.assignment.service.order.OrderQueryResponse;
import test.assignment.service.order.OrderResponse;
import test.assignment.service.order.OrderService;

import java.net.URI;
import java.time.ZonedDateTime;

@Tag(name = "order", description = "Order operations")
@RequiredArgsConstructor
@RestController
@RequestMapping(path = "/order")
public class OrderController {

    private final OrderService orderService;

    @Operation(summary = "Creates an order",
            description = "Creates an order from a buyer's email and a list of product IDs, computes and saves the order total price.",
            responses = {@ApiResponse(responseCode = "201",
                    description = "Order successfully created.",
                    content = @Content(schema = @Schema()),
                    headers = @Header(name = "Location", description = "Absolute URL of the created order.")),
                    @ApiResponse(responseCode = "400", description = "Missing attributes in the request payload or invalid buyer email.",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = CheckedErrorResponse.class))),
                    @ApiResponse(responseCode = "404", description = "Product id doesn't exist.",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = CheckedErrorResponse.class)))})
    @PostMapping(consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Void> createOrder(@RequestBody CreateOrderRequest createOrderRequest)
            throws MissingAttributeException, ResourceNotFoundException, InvalidCurrency, InvalidAttributeException {
        Order order = orderService.createOrder(createOrderRequest);
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{id}")
                .buildAndExpand(order.getId()).toUri();

        return ResponseEntity.created(location).build();
    }

    @Operation(summary = "Retrieves order by ID.",
            description = "Retrieves order by ID.")
    @GetMapping(produces = "application/json", path = "{id}")
    @ResponseStatus(HttpStatus.OK)
    public OrderResponse getOrder(@PathVariable("id") Long id) throws ResourceNotFoundException {
        return orderService.getOrder(id);
    }

    @Operation(summary = "Finds orders by time period.",
            description = "Returns the orders created between two given dates. The dates should be strings in ISO format. Orders are listed in no particular order.")
    @GetMapping(produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public OrderQueryResponse findOrderByTimePeriod(
            @Parameter(description = "Start date in ISO format.", example = "2020-10-26T21:05:13.542+01:00")
            @RequestParam("fromDate")
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
                    ZonedDateTime fromDate,
            @Parameter(description = "End date in ISO format.", example = "2020-10-27T21:05:13.542+01:00")
            @RequestParam("toDate")
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
                    ZonedDateTime toDate) {
        return orderService.findOrderByTimePeriod(fromDate, toDate);
    }

}
