package test.assignment.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.headers.Header;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import test.assignment.service.product.Product;
import test.assignment.service.*;
import test.assignment.service.product.CreateModifyProductRequest;
import test.assignment.service.product.ProductQueryResponse;
import test.assignment.service.product.ProductService;

import java.net.URI;

@Tag(name = "product", description = "Product operations")
@RequiredArgsConstructor
@RestController
@RequestMapping(path = "/product")
public class ProductController {

    private final ProductService productService;

    @Operation(summary = "Creates a product",
            description = "Creates a new product from name and price. Returns location of newly created product in HTTP header.",
            tags = "product",
            responses = {@ApiResponse(responseCode = "201",
                    description = "Product successfully created.",
                    content = @Content(schema = @Schema()),
                    headers = @Header(name = "Location", description = "Absolute URL of created product.")),
                    @ApiResponse(responseCode = "400", description = "Missing attributes in the request payload.",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = CheckedErrorResponse.class)))})
    @PostMapping(consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity createProduct(@RequestBody CreateModifyProductRequest productRequest) throws MissingAttributeException {
        final Product product = productService.createProduct(productRequest);
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{id}")
                .buildAndExpand(product.getId()).toUri();

        return ResponseEntity.created(location).build();
    }

    @Operation(summary = "Modifies an existing product.",
            description = "Modifies an existing product. Fails if product doesn't exist",
            tags = "product",
            responses = {@ApiResponse(responseCode = "204",
                    description = "Product successfully modified.",
                    content = @Content(schema = @Schema())),
                    @ApiResponse(responseCode = "400", description = "Missing attributes in the request payload.",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = CheckedErrorResponse.class))),
                    @ApiResponse(responseCode = "404", description = "Product was not found.",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = CheckedErrorResponse.class)))})
    @PutMapping(consumes = "application/json", path = "{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void modifyProduct(@Parameter(description = "ID of the product to modify") @PathVariable("id") Long id, @RequestBody CreateModifyProductRequest productRequest)
            throws ResourceNotFoundException, MissingAttributeException {
        productService.modifyProduct(id, productRequest);
    }

    @Operation(summary = "Returns all products",
            description = "Returns all products in no particular order",
            tags = "product")
    @GetMapping(produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public ProductQueryResponse getAllProducts() {
        return productService.getAllProducts();
    }
}
