package test.assignment.controller;

public class GenericErrorResponse extends ErrorResponse {

    public GenericErrorResponse(String message) {
        super("internal.error", message, null);
    }
}
