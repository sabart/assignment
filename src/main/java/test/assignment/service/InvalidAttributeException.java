package test.assignment.service;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class InvalidAttributeException extends CheckedException {

    private final String attribute;
}
