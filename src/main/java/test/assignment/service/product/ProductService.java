package test.assignment.service.product;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import test.assignment.service.MissingAttributeException;
import test.assignment.service.ResourceNotFoundException;

import java.util.Objects;

import static org.apache.commons.lang3.StringUtils.isBlank;

@RequiredArgsConstructor
@Component
public class ProductService {

    private final ProductRepository productRepository;

    @Transactional
    public Product createProduct(CreateModifyProductRequest productRequest) throws MissingAttributeException {

        Objects.requireNonNull(productRequest);

        checkProductAttributes(productRequest);

        final Product product = new Product(null, productRequest.getName(),productRequest.getPrice());

        return productRepository.save(product);
    }

    @Transactional
    public void modifyProduct(Long id, CreateModifyProductRequest productRequest) throws ResourceNotFoundException, MissingAttributeException {
        Objects.requireNonNull(productRequest);

        checkProductAttributes(productRequest);

        if (productRepository.existsById(id)) {
            final Product product = new Product(id, productRequest.getName(),productRequest.getPrice());
            productRepository.save(product);
        } else {
            throw new ResourceNotFoundException("product", id);
        }

    }

    @Transactional
    public ProductQueryResponse getAllProducts() {

        return new ProductQueryResponse(productRepository.findByOrderByName());
    }

    private void checkProductAttributes(CreateModifyProductRequest productRequest) throws MissingAttributeException {
        if (isBlank(productRequest.getName())) {
            throw new MissingAttributeException("product.name");
        } else if (productRequest.getPrice() == null) {
            throw new MissingAttributeException("product.price");
        } else if (productRequest.getPrice().getAmount() == null) {
            throw new MissingAttributeException("product.price.amount");
        } else if (isBlank(productRequest.getPrice().getCurrency())) {
            throw new MissingAttributeException("product.price.currency");
        }
    }

}
