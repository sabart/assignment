package test.assignment.service.product;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
@Getter
public class ProductQueryResponse {
    private final List<Product> products;
}
