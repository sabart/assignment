package test.assignment.service.product;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import test.assignment.service.Money;
import test.assignment.service.order.Order;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity
@Table(name = "products")
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@ToString(exclude = "orders")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @JsonIgnore
    @ManyToMany(mappedBy = "products")
    private Set<Order> orders;

    @NotNull
    private String name;

    @NotNull
    private Money price;

    public Product(Long id, @NotNull String name, @NotNull Money price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }
}
