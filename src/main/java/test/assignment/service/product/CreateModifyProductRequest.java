package test.assignment.service.product;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import test.assignment.service.Money;

/**
 * DTO for creating and modifying a product record.
 */
@RequiredArgsConstructor
@Getter
public class CreateModifyProductRequest {

    private final String name;

    private final Money price;

}
