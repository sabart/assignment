package test.assignment.service;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class InvalidCurrency extends CheckedException{

    private final Long productId;

    private final String invalidCurrency;

    private final String validCurrency;
}
