package test.assignment.service.order;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import test.assignment.service.Money;
import test.assignment.service.product.Product;
import test.assignment.service.InvalidAttributeException;
import test.assignment.service.InvalidCurrency;
import test.assignment.service.MissingAttributeException;
import test.assignment.service.ResourceNotFoundException;
import test.assignment.service.product.ProductRepository;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Pattern;

import static org.apache.commons.lang3.StringUtils.isBlank;

@RequiredArgsConstructor
@Component
public class OrderService {

    public static final Pattern EMAIL_PATTERN =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    private static final String ORDER_CURRENCY = "EUR";

    private final OrderRepository orderRepository;

    private final ProductRepository productRepository;

    @Transactional
    public Order createOrder(CreateOrderRequest createOrderRequest)
            throws MissingAttributeException, ResourceNotFoundException,
            InvalidCurrency, InvalidAttributeException {

        Objects.requireNonNull(createOrderRequest);

        if (isBlank(createOrderRequest.getBuyerEmail())) {
            throw new MissingAttributeException("order.buyerEmail");
        } else if (CollectionUtils.isEmpty(createOrderRequest.getProductIds())) {
            throw new MissingAttributeException("order.productIds");
        } else if (!EMAIL_PATTERN.matcher(createOrderRequest.getBuyerEmail()).matches()) {
            throw new InvalidAttributeException("order.buyerEmail");
        }

        List<Product> products = new ArrayList<>();

        // Check all products exist and currency is the same.
        for (Long id : createOrderRequest.getProductIds()) {
            final Optional<Product> productOpt = productRepository.findById(id);

            if (productOpt.isPresent()) {
                final Product product = productOpt.get();
                if (!product.getPrice().getCurrency().equals(ORDER_CURRENCY)) {
                    throw new InvalidCurrency(id, product.getPrice().getCurrency(), ORDER_CURRENCY);
                }
                products.add(product);
            } else {
                throw new ResourceNotFoundException("product", id);
            }
        }

        // Compute order total price.
        final BigDecimal totalPrice = products.stream().map(p -> p.getPrice().getAmount()).reduce(BigDecimal.ZERO, BigDecimal::add);

        final Order order = new Order(createOrderRequest.getBuyerEmail(), ZonedDateTime.now(), products, new Money(totalPrice, ORDER_CURRENCY));

        return orderRepository.save(order);
    }


    public OrderResponse getOrder(Long id) throws ResourceNotFoundException {

        Objects.requireNonNull(id);

        final Optional<Order> order = orderRepository.findById(id);

        if (!order.isPresent()) {
            throw new ResourceNotFoundException("order", id);
        } else {
            return new OrderResponse(order.get());
        }

    }

    public OrderQueryResponse findOrderByTimePeriod(ZonedDateTime fromDate, ZonedDateTime toDate) {

        Objects.requireNonNull(fromDate);
        Objects.requireNonNull(toDate);

        return new OrderQueryResponse(orderRepository.findAllByCreatedDateBetween(fromDate, toDate));

    }
}
