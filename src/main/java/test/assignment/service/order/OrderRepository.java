package test.assignment.service.order;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

public interface OrderRepository extends CrudRepository<Order, Long> {

    @EntityGraph("order-entity-graph")
    @Override
    Optional<Order> findById(Long aLong);

    @EntityGraph("order-entity-graph")
    List<Order> findAllByCreatedDateBetween(ZonedDateTime fromDate, ZonedDateTime toDate);
}
