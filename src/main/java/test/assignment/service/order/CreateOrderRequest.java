package test.assignment.service.order;


import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.List;


@RequiredArgsConstructor
@Getter
public class CreateOrderRequest {

    private final String buyerEmail;

    private final List<Long> productIds;
}
