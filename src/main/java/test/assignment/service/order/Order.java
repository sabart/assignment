package test.assignment.service.order;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import test.assignment.service.Money;
import test.assignment.service.product.Product;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedSubgraph;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;
import java.util.List;

@NamedEntityGraph(
        name = "order-entity-graph",
        attributeNodes = {
                @NamedAttributeNode("buyerEmail"),
                @NamedAttributeNode("products"),
                @NamedAttributeNode("totalPrice"),
                @NamedAttributeNode("createdDate")
        },
        subgraphs = {
                @NamedSubgraph(
                        name = "products-subgraph",
                        attributeNodes = {
                                @NamedAttributeNode("id"),
                                @NamedAttributeNode("name"),
                                @NamedAttributeNode("price")
                        }
                )
        }
)
@Entity
@Table(name = "orders")
@NoArgsConstructor
@Getter
@ToString
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    private String buyerEmail;

    @NotNull
    private ZonedDateTime createdDate;

    @NotNull
    @ManyToMany
    @JoinTable(
            name = "product_orders",
            joinColumns = @JoinColumn(name = "order_id"),
            inverseJoinColumns = @JoinColumn(name = "product_id"))
    private List<Product> products;

    @NotNull
    private Money totalPrice;

    public Order( @NotNull String buyerEmail, @NotNull ZonedDateTime createdDate, @NotNull List<Product> products, @NotNull Money price) {
        this(null, buyerEmail,createdDate, products, price);
    }


    public Order(@NotNull Long id, @NotNull String buyerEmail, @NotNull ZonedDateTime createdDate, @NotNull List<Product> products, @NotNull Money price) {
        this.id = id;
        this.buyerEmail = buyerEmail;
        this.createdDate = createdDate;
        this.products = products;
        this.totalPrice = price;
    }
}
