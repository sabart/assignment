package test.assignment.service.order;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class OrderResponse {
    private final Order order;
}
