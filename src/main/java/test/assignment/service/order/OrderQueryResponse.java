package test.assignment.service.order;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
@Getter
public class OrderQueryResponse {
    private final List<Order> orders;
}
