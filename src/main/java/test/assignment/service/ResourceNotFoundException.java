package test.assignment.service;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class ResourceNotFoundException extends CheckedException{

    private final String type;

    private final Long id;
}
