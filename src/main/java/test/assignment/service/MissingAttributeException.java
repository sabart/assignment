package test.assignment.service;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class MissingAttributeException extends CheckedException {

    private final String attribute;
}
