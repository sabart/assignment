package test.assignment;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OpenApiConfig {


    /**
     * The version of DataPlatform API
     */
    private final String apiVersion;

    /**
     * Takes API version and server urls from environment properties as argument parameters.
     *
     * @param apiVersion The version of DataPlatform API.
     */
    public OpenApiConfig(@Value("${api.version:v?.?}") String apiVersion) {

        this.apiVersion = apiVersion;
    }

    /**
     * Custom OpenAPi configuration bean.
     *
     * @return the configuration bean
     */
    @Bean
    public OpenAPI customOpenAPI() {


        OpenAPI openAPI = new OpenAPI()
                .info(new Info().version(apiVersion).title("Assignment Products API").description(
                        "Assignment Products API"));

        return openAPI;
    }


}
